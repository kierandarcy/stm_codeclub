from microbit import *
from random import randint

while True:
    # Pause for 1 to 5 seconds.
    sleep(randint(1000, 5000))

    # Show a happy face.
    display.show(Image.HAPPY)

    # When no buttons are being pressed, wait.
    while not button_a.is_pressed() and not button_b.is_pressed():
        sleep(20)

    if button_a.is_pressed():
        display.show(Image.ARROW_W)
    
    if button_b.is_pressed():
        display.show(Image.ARROW_E)

    # Pause for a second
    sleep(1000)

    # Clear the screen
    display.clear()
