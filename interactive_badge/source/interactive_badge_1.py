from microbit import *

feeling = Image.ASLEEP

while True:
    if button_a.is_pressed():
        feeling = Image.HAPPY
    elif button_b.is_pressed():
        feeling = Image.SAD

    display.show(feeling)
