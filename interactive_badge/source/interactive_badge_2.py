from microbit import *

feeling = Image.ASLEEP

while True:
    if button_a.is_pressed():
        feeling = Image.HAPPY
    elif button_b.is_pressed():
        feeling = Image.SAD

    if accelerometer.was_gesture('face down'):
        feeling = Image.ASLEEP
    elif accelerometer.was_gesture('shake'):
        feeling = Image.SURPRISED

    display.show(feeling)
