import json
from urllib.request import urlopen

url = 'http://api.open-notify.org/astros.json'

data = json.loads(urlopen(url).read())

print("There are {} people in space".format(data['number']))

for person in data['people']:
    print("{} is flying in the {}".format(person['name'], person['craft']))
