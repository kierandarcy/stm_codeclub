data = {
   "number": 2,
   "people": [
      {
          "name": "Santa", 
          "craft": "Christmas Sleigh"
      },
      {
          "name": "Rudolph",
          "craft": "Christmas Sleigh"
      },
    ]
}

print("There are {} people in space".format(data['number']))

for person in data['people']:
    print("{} is flying in the {}".format(person['name'], person['craft']))
