from microbit import *

tree = [
    Image.XMAS * (i/9) 
    for i 
    in range(9, -1, -1)
]

display.show(tree, delay=100, wait=False)
