import radio
from microbit import *

tree = [
    Image.XMAS * (i/9) 
    for i 
    in range(9, -1, -1)
]

# We have to turn our radio on first
radio.on()

while True:
    # Button A sends a 'flash' message.
    if button_a.was_pressed():
        display.show(tree, delay=100, wait=False)
        radio.send('flash')

    # Read any incoming messages.
    incoming = radio.receive()

    # If our message says 'flash', then show our image
    if incoming == 'flash':
        display.show(tree, delay=100, wait=False)
