from microbit import *

tree = [
    Image.XMAS * (i/9) 
    for i 
    in range(9, -1, -1)
]

while True:
    # Button A was pressed show our tree
    if button_a.was_pressed():
        display.show(tree, delay=100, wait=False)
